# text-adventure

Videogame created with Unity and C#. "Text adventure" is a text-based game that uses a text-based interface in which you start reading a story given by a narrator and you have to choose between several options that will continue with the story.